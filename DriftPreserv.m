%--- Settings ----------------------------------------------------%
randn('state',0) % fix the initial state to get repeatable results
%randn('state',sum(100*clock)) %different seeds
T0 = 0; T = 100; 
h = 0.1; %stepsize
N = round((T - T0) / h); % number of steps
tol=1e-8; %accuracy of Newton Iteration
x0 = 1; %Inital value of Newton Iteration
NUMSIM = 5000; % the number of desired simulations

%-------------------------------------------------------------------%

NORMRAND = randn(N,NUMSIM); % an (N+1)x(NUMSIM) matrix...
% of (pseudo)random entries...
% from N(0;1)
q = zeros(N,NUMSIM);
p = zeros(N,NUMSIM);
q_EM = zeros(N,NUMSIM);
p_EM = zeros(N,NUMSIM);
Phi = zeros(1,NUMSIM);
dW = zeros(N,NUMSIM);
H = zeros(N,1);
H_EM = zeros(N,1);

q(1,:) = 1; %Initial conditions
p(1,:) = 0;
q_EM(1,:) = 1; %Initial conditions
p_EM(1,:) = 0;
Sigma = 1;
H_tem = Hamiltonian(p(1,:),q(1,:));
H(1,:) = sum(H_tem)/NUMSIM;
H_EM(1,:) = sum(H_tem)/NUMSIM;



for(i=1:NUMSIM) %Computation of Wiener process (ith simulation)
    dW(:,i) = sqrt(h)*NORMRAND(:,i); 
end


for(i=2:N) %The Integrator
    %Phi = Newton(@fct,@dfct, p(i-1,:), q(i-1,:), h, Sigma, dW(i-1,:), x0, tol);
    Phi = (p(i-1,:) + Sigma*dW(i-1,:) - h/2*q(i-1,:))/(1+h^2/4);
    q(i,:) = q(i-1,:) + h*Phi;
    %p(i,:) = p(i-1,:) + Sigma*dW(i-1,:) - h*( ( cos(q(i-1,:)) -cos(q(i-1,:) + ...
        %h*Phi) )./( h*Phi) ); 
    p(i,:) = p(i-1,:) + Sigma*dW(i-1,:) - h*(q(i-1,:) + h/2*Phi);
    H_tem = Hamiltonian(p(i,:),q(i,:));
    H(i,:) = sum(H_tem)/NUMSIM;
end
for(i=2:N)
    p(i,:) = sum(p(i,:))/NUMSIM;
    q(i,:) = sum(q(i,:))/NUMSIM;
end


for(i=2:N)
    q_EM(i,:) = q_EM(i-1,:) + p_EM(i-1,:)*h;
    p_EM(i,:) = p_EM(i-1,:) - q_EM(i-1,:)*h + Sigma*dW(i-1,:);
    H_tem = Hamiltonian(p_EM(i,:),q_EM(i,:));
    H_EM(i,:) = sum(H_tem)/NUMSIM;
end
for(i=2:N)
    p_EM(i,:) = sum(p_EM(i,:))/NUMSIM;
    q_EM(i,:) = sum(q_EM(i,:))/NUMSIM;
end

figure
hold on
set(gca,'FontSize',22)
plot([T0:h:T-h],H,'b.')
plot([T0:h:T-h],H(1,:)+1/2*Sigma^2*[T0:h:T-h],'r.')
hold off
xlabel('t')
ylabel('Hamiltonian')
title([ {'Average of Numerical Hamiltonian and Expectation of the Hamiltonian',' with the Drift Preserving Scheme'} ])
subtitle(['(h = ' num2str(h) ', number of simulations = ' num2str(NUMSIM) ')'])
legend('Numerical Hamiltonian','Expectation of Hamiltonian','Location','northwest')

figure
hold on
set(gca,'FontSize',22)
plot(q,p,'b')
xlabel('q')
ylabel('p')
title([ {'Phase Space of Average Position and Momentum',' With the Drift Preserving Scheme' }])
subtitle(['(h = ' num2str(h) ', number of simulations = ' num2str(NUMSIM) ')'])
axis([-1.2,1.2, -1.2, 1.2])
hold off
%legend('Numerical Hamiltonian','Expectation of Hamiltonian','Location','northwest')

figure
hold on
set(gca,'FontSize',22)
plot(log10([T0:h:T-h]),log10(H_EM),'b.')
plot(log10([T0:h:T-h]),log10(H_EM(1,:)+1/2*Sigma^2*[T0:h:T-h]),'r.')
hold off
xlabel('log_{10}(t)')
ylabel('log_{10}(Hamiltonian)')
title([ {'Average of Numerical Hamiltonian and Expectation of the Hamiltonian',' with the Euler-Maruyama Scheme'} ])
subtitle(['(h = ' num2str(h) ', number of simulations = ' num2str(NUMSIM) ')'])
legend('Numerical Hamiltonian','Expectation of Hamiltonian','Location','northwest')

figure
hold on
set(gca,'FontSize',22)
plot(q_EM,p_EM,'b')
hold off
xlabel('q')
ylabel('p')
title([{ 'Phase Space of Average Position and Momentum',' with the Euler-Maruyama Scheme'} ])
subtitle(['(h = ' num2str(h) ', number of simulations = ' num2str(NUMSIM) ')'])
%-------------------------------------------------------------------%
%definition of the Hamiltonian
function H = Hamiltonian(p,q)
    %H = 1/2*p.^2 - cos(q);
    H = 1/2*p.^2 + 1/2*q.^2;
end
%-------------------------------------------------------------------%
%definition of exact q
function q = q_exact(q0,p0,t,h,dW)
    sum = 0;
    for i = 1:t/h
        sum = sum + sin(t-i*h)*dW(i)
    end
    q = q0 * cos(t) + p0*sin(t) + h*sum;
end

%-------------------------------------------------------------------%
%definition of exact p
%-------------------------------------------------------------------%
%definition of function for Newton Iteration
function f = fct(p,q,h,Sigma,dW,Phi)
    f = Phi - p - Sigma*dW + h/2*( (cos(q) - cos(q + h*Phi) )./(h*Phi) );
end
%-------------------------------------------------------------------%
%definition of derivative for Newton Iteration
function df = dfct(p,q,h,Sigma,dW,Phi)
    df = 1 + h/2*( h*Phi.*sin(q + h*Phi) + cos(q + h*Phi) - cos(q) )./(h*Phi.^2);
end
%-------------------------------------------------------------------%
%Newton Iteration
function zero = Newton(fct,dfct, p, q, h, Sigma, dW, x0, tol)
maxIter = 50;
Iter = 0;
x=x0;
while( (abs( feval(@fct, p,q,h,Sigma,dW,x) )>tol) & (Iter < maxIter) )
    x = x - feval(@fct, p,q,h,Sigma,dW,x) ./ feval(@dfct, p,q,h,Sigma,dW,x);
    Iter = Iter+1;
    if(Iter == maxIter)
        disp('fertig')
    end
end
zero = x;
end