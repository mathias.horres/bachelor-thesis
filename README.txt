The DriftPreserv.m file contains the graphs for the stochastic oscillator with the Euler-Maruyama and drift-preserving scheme

The euler_Graphs.m file produces the graphs for the euler methods based on the https://de.mathworks.com/matlabcentral/fileexchange/7686-symplectic-integrators files

The kepler_high_order.m file produces the graphs for the ode89 and gauss method with the gnicodes files 
