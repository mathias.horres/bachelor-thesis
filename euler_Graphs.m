%Initial conditions
e = 0.6;
q1_0 = [1-e];
q2_0 = [0];
p1_0 = [0];
p2_0 = [sqrt( (1+e)/(1-e) )];

% Exact solution
L_0 = q1_0*p2_0 - q2_0*p1_0;
H_0 = H(q1_0,q2_0,p1_0,p2_0);
d = L_0^2;

%--------------------------------------------------------------------------
% Time span
h=0.01;
N=629*15;
t=0:h:h*N;

q = zeros(N+1,2);
p = zeros(N+1,2);
q(1,1) = q1_0;
q(1,2) = q2_0;
p(1,1) = p1_0;
p(1,2) = p2_0;

for i = 2:N+1
    [q(i,:) p(i,:)] = phiKepler(q(i-1,:),p(i-1,:),h); %exact solution
end

[q1,q2,p1,p2]=symEu_q(@Xq1,@Xq2,@Xp1,@Xp2,t,q1_0,q2_0,p1_0,p2_0);%numerical solution symplectic
[qe1,qe2,pe1,pe2]=euler(@Xq1,@Xq2,@Xp1,@Xp2,t,q1_0,q2_0,p1_0,p2_0); %explicit Euler

q_tilde = [q1,q2];
p_tilde = [p1,p2];

qe_tilde = [qe1,qe2];
pe_tilde = [pe1,pe2];

figure
hold on
set(gca,'FontSize',22)
plot(q1,q2,'b.')
plot(q(:,1),q(:,2),'r.')
xlabel('q1'), ylabel('q2')
title(['Symplectic Euler, (h = ' num2str(h) ', N = ' num2str(N+1) ')'])
legend('Numerical Solution','Exact Solution')
hold off

figure
hold on
set(gca,'FontSize',22)
plot(q1,p1,'b.')
plot(q2,p2,'g.')
plot(q(:,1),p(:,1),'r.')
plot(q(:,2),p(:,2),'m.')
xlabel('q'), ylabel('p')
title(['Symplectic Euler Method, (h = ' num2str(h) ', N = ' num2str(N+1) ')'])
legend('Numerical q1,p1','Numerical q2,p2','Exact q1,p1','Exact q2,p2','Location','northwest')
hold off

figure
hold on
set(gca,'FontSize',22)
plot(qe1,qe2,'b.')
plot(q(:,1),q(:,2),'r.')
xlabel('q1'), ylabel('q2')
title(['Explicit Euler Method, (h = ' num2str(h) ', N = ' num2str(N+1) ')'])
legend('Numerical Solution','Exact Solution')
hold off

figure
hold on
set(gca,'FontSize',22)
plot(qe1,pe1,'b.')
plot(qe2,pe2,'g.')
plot(q(:,1),p(:,1),'r.')
plot(q(:,2),p(:,2),'m.')
xlabel('q'), ylabel('p')
axis([-7 4 -1.5 2.5])
title(['Explicit Euler Method, (h = ' num2str(h) ', N = ' num2str(N+1) ')'])
legend('Numerical q1,p1','Numerical q2,p2','Exact q1,p1','Exact q2,p2','Location','northwest')
hold off



figure
hold on
set(gca,'FontSize',22)
err_q=zeros(N+1,1);
for i = 1:N+1
    err_q(i) = norm(q(i,:)-q_tilde(i,:));
end
plot(t, err_q,'k.')
title(['Error in Position q for Symplectic Euler Method'])
xlabel('t'), ylabel('Error in q')
hold off

figure
hold on
set(gca,'FontSize',22)
err_phase_sym=zeros(N+1,1);
for i = 1:N+1
    err_phase_sym(i) = norm([q(i,1),q(i,2),p(i,1),p(i,2)] - [q_tilde(i,1),q_tilde(i,2),p_tilde(i,1),p_tilde(i,2)]);
end
plot(t, err_phase_sym,'k.')
title(['Error in Norm of Phasespace for Symplectic Euler Method'])
xlabel('t'), ylabel('Error in Norm(q,p)')
hold off

figure
hold on
set(gca,'FontSize',22)
err_q=zeros(N+1,1);
for i = 1:N+1
    err_q(i) = norm(q(i,:)-qe_tilde(i,:));
end
plot(t, err_q,'k.')
title(['Error in Position q for Explicit Euler Method'])
xlabel('t'), ylabel('Error in q')
hold off

figure
hold on
set(gca,'FontSize',22)
err_phase_nonsym=zeros(N+1,1);
for i = 1:N+1
    err_phase_nonsym(i) = norm([q(i,1),q(i,2),p(i,1),p(i,2)] - [qe_tilde(i,1),qe_tilde(i,2),pe_tilde(i,1),pe_tilde(i,2)]);
end
plot(t, err_phase_nonsym,'k.')
title(['Error in Norm of Phasespace for Explicit Euler Method'])
xlabel('t'), ylabel('Error in Norm(q,p)')
hold off



figure
hold on
set(gca,'FontSize',22)
plot(t,H(q1,q2,p1,p2)-H_0,'k.')
title(['Error in Hamiltonian for Symplectic Euler Method'])
xlabel('t'), ylabel('Error in H(q,p)')
hold off

figure
hold on
set(gca,'FontSize',22)
plot(t,L(q1,q2,p1,p2)-L_0,'k.')
title(['Error in Angular Momentum for Symplectic Euler Method'])
xlabel('t'), ylabel('Error in L(q,p)')
hold off

figure
hold on
set(gca,'FontSize',22)
plot(t,H(qe1,qe2,pe1,pe2)-H_0,'k.')
title(['Error in Hamiltonian for Explicit Euler Method'])
xlabel('t'), ylabel('Error in H(q,p)')
hold off

figure
hold on
set(gca,'FontSize',22)
plot(t,L(qe1,qe2,pe1,pe2)-L_0,'k.')
title(['Error in Angular Momentum for Explicit Euler Method'])
xlabel('t'), ylabel('Error in L(q,p)')
hold off
%--------------------------------------------------------------------------
function out = H(q1,q2,p1,p2)
    out = 1/2*(p1.^2 + p2.^2) - 1./sqrt(q1.^2 + q2.^2);
end
%--------------------------------------------------------------------------
function out = L(q1,q2,p1,p2)
    out = q1.*p2-q2.*p1;
end
%--------------------------------------------------------------------------
function out=Xq1(p1,p2)
    out=p1;
end
%--------------------------------------------------------------------------
function out=Xq2(p1,p2)
out=p2;
end
%--------------------------------------------------------------------------
function out=Xp1(q1,q2)
    out=q1/(q1^2+q2^2)^(3/2);
end
%--------------------------------------------------------------------------
function out=Xp2(q1,q2)
    out=q2/(q1^2+q2^2)^(3/2); 
end