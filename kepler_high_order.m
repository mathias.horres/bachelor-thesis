%Initial conditions
e = 0.6;
q1_0 = [1-e];
q2_0 = [0];
p1_0 = [0];
p2_0 = [sqrt( (1+e)/(1-e) )];

% Numerical solution symplectic
L_0 = q1_0*p2_0 - q2_0*p1_0;
H_0 = H(q1_0,q2_0,p1_0,p2_0);
d = L_0^2;
phi = 0:pi/100:2*pi;
tmax = 1000*pi;
h=0.05;
tspan=[0,tmax];

[out,out2,out3] = gni_irk2('kepler',tspan,[],[],0.6);

%exact solution
N = length(out2)
q = zeros(N,2);
p = zeros(N,2);
q(1,1) = q1_0;
q(1,2) = q2_0;
p(1,1) = p1_0;
p(1,2) = p2_0;
for i = 2:N
    [q(i,:) p(i,:)] = phiKepler(q(i-1,:),p(i-1,:),h);
end

%ODE89
tspan = [0:h:tmax];
y0 = [1-e,0,0,sqrt((1+e)/(1-e))];
[t,y] = ode89(@twobodyode, tspan, y0);

figure
hold on
set(gca,'FontSize',22)
plot(out2(:,1),out2(:,2),'b.')
plot(q(:,1),q(:,2),'r.')
title(['Gauss Method of Order 8, (h = ' num2str(h) ', N = ' num2str(N) ')'])
xlabel('q1'), ylabel('q2')
legend('Numerical Solution','Exact Solution')
hold off


figure
hold on
set(gca,'FontSize',22)
plot(out2(:,1),out3(:,1),'b.')
plot(out2(:,2),out3(:,2),'g.')
plot(q(:,1),p(:,1),'.r')
plot(q(:,2),p(:,2),'.m')
xlabel('q'), ylabel('p')
title(['Gauss Method of Order 8, (h = ' num2str(h) ', N = ' num2str(N) ')'])
legend('Numerical q1,p1','Numerical q2,p2','Exact q1,p1','Exact q2,p2','Location','northwest')
hold off

figure
hold on
set(gca,'FontSize',22)
plot(y(:,1),y(:,3),'b.')
plot(q(:,1),q(:,2),'r.')
title(['ode89, (h = ' num2str(h) ', N = ' num2str(N) ')'])
xlabel('q1'), ylabel('q2')
legend('Numeric Solution','Exact Solution')
hold off

figure
hold on
set(gca,'FontSize',22)
plot(y(:,1),y(:,2),'b.')
plot(y(:,3),y(:,4),'g.')
plot(q(:,1),p(:,1),'.r')
plot(q(:,2),p(:,2),'.m')
xlabel('q'), ylabel('p')
title(['ode89, (h = ' num2str(h) ', N = ' num2str(N) ')'])
legend('Numerical q1,p1','Numerical q2,p2','Exact q1,p1','Exact q2,p2','Location','northwest')
hold off


figure
hold on
set(gca,'FontSize',22)
err_q=zeros(N,1);
for i = 1:N
    err_q(i) = norm(q(i,:)-out2(i,:));
end
%plot(tspan, err_q,'k.')
plot(err_q,'k.')
title(['Error in Position q for Gauss Method of Order 8'])
xlabel('t'), ylabel('Error in q')
xlim([0,N])
%axis([0 100 0 10^(-9)])
hold off

figure
hold on
set(gca,'FontSize',22)
err_phase_sym=zeros(N,1);
for i = 1:N
    err_phase_sym(i) = norm([q(i,1),q(i,2),p(i,1),p(i,2)] - [out2(i,1),out2(i,2),out3(i,1),out3(i,2)]);
end
plot(err_phase_sym,'k.')
title(['Error in Phase Space for Gauss Method of Order 8'])
xlabel('t'), ylabel('Error in (q,p)')
xlim([0,N])
hold off

figure
hold on
set(gca,'FontSize',22)
err_q=zeros(N,1);
for i = 1:N-1
    err_q(i) = norm(q(i,:)-[y(i,1),y(i,3)]);
end
plot(err_q,'k.')
title(['Error in Position q for ode89'])
xlabel('t'), ylabel('Error in q')
xlim([0,N])
hold off

figure
hold on
set(gca,'FontSize',22)
err_phase_nonsym=zeros(N,1);
for i = 1:N-1
    err_phase_nonsym(i) = norm([q(i,1),q(i,2),p(i,1),p(i,2)] - [y(i,1),y(i,3),y(i,2),y(i,4)]);
end
plot(err_phase_nonsym,'k.')
title(['Error in Phase Space for ode89'])
xlabel('t'), ylabel('Error in (q,p)')
xlim([0,N])
hold off



figure
hold on
set(gca,'FontSize',22)
plot(H(out2(:,1),out2(:,2),out3(:,1),out3(:,2))-H_0,'k')
title(['Error in Hamiltonian for Gauss Method of Order 8'])
xlabel('t'), ylabel('Error in H(q,p)')
xlim([0,N])
hold off

figure
hold on
set(gca,'FontSize',22)
plot(L(out2(:,1),out2(:,2),out3(:,1),out3(:,2))-L_0,'k.')
title(['Error in Angular Momentum for Gauss Method of Order 8'])
xlabel('t'), ylabel('Error in L(q,p)')
xlim([0,N])
hold off

figure
hold on
set(gca,'FontSize',22)
plot(H(y(:,1),y(:,3),y(:,2),y(:,4))-H_0,'k.')
title(['Error in Hamiltonian for ode89'])
xlabel('t'), ylabel('Error in H(q,p)')
xlim([0,N])
hold off

figure
hold on
set(gca,'FontSize',22)
plot(L(y(:,1),y(:,3),y(:,2),y(:,4))-L_0,'k.')
title(['Error in Angular Momentum for ode89'])
xlabel('t'), ylabel('Error in L(q,p)')
xlim([0,N])
hold off

%--------------------------------------------------------------------------
function out = H(q1,q2,p1,p2)
out = 1/2*(p1.^2 + p2.^2) - 1./sqrt(q1.^2 + q2.^2);
end
%--------------------------------------------------------------------------
function out = L(q1,q2,p1,p2)
    out = q1.*p2-q2.*p1;
end
%--------------------------------------------------------------------------
function out = r(d,e,phi)
out = d./(1+e*cos(phi)).';
end
%--------------------------------------------------------------------------
%From https://de.mathworks.com/help/matlab/ref/ode89.html%
%--------------------------------------------------------------------------
function dy = twobodyode(t,y)
% Two-body problem with one mass much larger than the other.
r = sqrt(y(1)^2 + y(3)^2);
dy = [y(2); 
     -y(1)/r^3;
      y(4);
     -y(3)/r^3];
end